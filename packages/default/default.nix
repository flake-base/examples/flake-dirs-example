{ inputs', library, pkgs }:

let

  inherit (pkgs) callPackage;

in
{

  hello = callPackage ./hello { inherit inputs' library; };

}
