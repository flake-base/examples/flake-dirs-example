{ library, packages }:

{

  # to test accessing other packages in the same flake
  hi = packages.hello.overrideAttrs (oldAttrs: {
    # to test library from the same flake
    pname = library.nested.hi;
    passthru.exePath = "/bin/hello";
  });

}
