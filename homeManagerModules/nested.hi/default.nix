{ attrsetPath, packages, config, lib, pkgs, ... }:

let

  cfg = lib.attrsets.attrByPath attrsetPath { } config;

in
{

  options = lib.attrsets.setAttrByPath attrsetPath {

    enable = lib.options.mkEnableOption "test module";

    package = lib.options.mkOption {
      type = lib.types.nullOr lib.types.package;
      default = packages.nested.hi;
      description = "Test package.";
    };

  };

  config = lib.mkIf cfg.enable {
    home.packages = [ cfg.package ];
  };

}
