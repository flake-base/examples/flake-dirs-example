{

  inputs.nixpkgs.url = "nixpkgs/release-22.05";

  inputs.flake-dir = {
    url = "gitlab:flake-base/flake-dirs";
    inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, flake-dir, ... } @ args: flake-dir args { };

}
