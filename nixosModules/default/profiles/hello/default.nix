args @ { inputs, packages, ... }:

let

  libFlakeDir = inputs.flake-dir.lib;

in
libFlakeDir.mkProfileModule args {

  environment.systemPackages = [ packages.hello ];

}
